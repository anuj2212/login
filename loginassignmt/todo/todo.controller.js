(function () {
    'use strict';

    angular
    .module('app')
    .controller('todoController', todoController);

    todoController.$inject = ['$scope','$filter'];

    function todoController($scope,$filter) {



        //todo table data

        $scope.$watch('newDate', function (newValue) {
           $scope.newDate = $filter('date')(newValue, 'dd/MM/yyyy'); 
        });



        $scope.allItems = getDummyData(); 

        $scope.resetAll = function()
        {
           $scope.filteredList = $scope.allItems ; 
           $scope.newTitle = '';
           $scope.newDate = '';
           $scope.newDescription = '';
           $scope.searchText = ''; 
        }





        $scope.add = function()
        {
           $scope.allItems.push({Title:$scope.newTitle, DaTe:$scope.newDate, Description:$scope.newDescription});
           $scope.resetAll();  
        }


        $scope.search = function()
        { 
            $scope.filteredList  = _.filter($scope.allItems,
               function(item){  
                   return searchUtil(item,$scope.searchText); 
               });

            if($scope.searchText == '')
            {
                $scope.filteredList = $scope.allItems ;
            }
        }  

        $scope.resetAll();       


        /* Search Text in all 3 fields */
        function searchUtil(item,toSearch)
        {

            return ( /*item.Title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||*/ item.DaTe.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 /*|| item.Description == toSearch */)? true : false ;
        }


        /*Get Dummy Data for Example*/
        function getDummyData()
        {
            return [
            {Title:'appointment', DaTe:'01/03/2017', Description: 'to see a dentist for jaw pain'},
            {Title:'to wash my car', DaTe:'07/03/2017', Description: 'wash car and take it for accessories'},

            ];
        }


}
})();

